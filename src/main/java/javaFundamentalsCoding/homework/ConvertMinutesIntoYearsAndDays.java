package javaFundamentalsCoding.homework;

import java.util.Scanner;

public class ConvertMinutesIntoYearsAndDays {
    public static void main(String[] args) {

       /* 2) Write a Java program to convert minutes into a number of years and days.

        Test Data
        Input the number of minutes: 3456789
        Expected Output :
        3456789 minutes is approximately 6 years and 210 days
       */

        //First we find out how many minutes are in a year

        long minutesInDays = 60 * 24;
        long minutesInYear = 60 * 24 * 365;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please insert a number in minutes: ");
        long minutes = scanner.nextLong();
        long remainingMinutes = minutes % minutesInYear;
        long year = minutes / minutesInYear;
        long days = remainingMinutes / minutesInDays;


        System.out.println(minutes + " is approximately " + year + " years and " +days + " days");






    }

}





